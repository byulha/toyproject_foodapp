from django.contrib import admin
from urllib.parse import quote
from .models import Category, Shop, Item
from django.utils.safestring import  mark_safe

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=['name', 'is_public','icon']


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    list_display= ('name', 'address_link')

    def address_link(self,shop):
        if shop.address:
            url='https://map.naver.com/?query=' + quote(shop.address)
            return mark_safe('<a href ="{}" target="_blank">{}</a>'.format(url,shop.address))
        return None


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ['shop', 'name']
    list_display_links = ['name']
    list_filter = ['shop']
    search_fields = ['name']
