from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from .models import Category, Shop, Review, Item
from .forms import ReviewForm
from django.shortcuts import render, get_object_or_404, redirect
from auth_mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin

# Create your views here.
index = ListView.as_view(model=Category)

category_detail = DetailView.as_view(model=Category)

shop_detail = DetailView.as_view(model=Shop)


class ReviewCreateView( LoginRequiredMixin, CreateView):
    model = Review
    form_class = ReviewForm

    def form_valid(self, form):
        self.shop = get_object_or_404(Shop, pk=self.kwargs['pk'])
        review = form.save(commit=False)
        review.shop = self.shop
        review.author = self.request.user  # django.contrib.auth.models.AnonymousUser

        return super().form_valid(form)

    def get_success_url(self):
        return self.shop.get_absolute_url()


review_new = ReviewCreateView.as_view( model= Review,form_class=ReviewForm)

